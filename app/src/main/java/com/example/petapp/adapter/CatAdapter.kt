package com.example.petapp.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.example.petapp.databinding.ItemCatBinding

class CatAdapter : RecyclerView.Adapter<CatAdapter.CatViewHolder>() {
    private var cat = mutableListOf<String>()

    class CatViewHolder(
        private val binding: ItemCatBinding,
    ) : RecyclerView.ViewHolder(binding.root) {
        fun getCats(catImages: String) {
            binding.ivCat.load(catImages)
        }

        companion object {
            fun newInstance(parent: ViewGroup) = ItemCatBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            ).let { CatViewHolder(it) }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CatViewHolder =
        CatViewHolder.newInstance(parent)


    override fun onBindViewHolder(holder: CatViewHolder, position: Int) {
        val cat = cat[position]
        holder.getCats(cat)
    }

    override fun getItemCount(): Int = cat.size

    fun displayCats(cat: List<String>) {
        this.cat.run {
            val oldSize = size
            clear()
            notifyItemRangeRemoved(0, oldSize)
            addAll(cat)
            notifyItemRangeInserted(0, size)
        }
    }
}