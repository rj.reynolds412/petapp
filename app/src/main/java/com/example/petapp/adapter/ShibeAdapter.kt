package com.example.petapp.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.example.petapp.databinding.ItemShibesBinding

class ShibeAdapter : RecyclerView.Adapter<ShibeAdapter.ShibeViewHolder>() {
    private var shibe = mutableListOf<String>()

    class ShibeViewHolder(
        private val binding: ItemShibesBinding,
    ) : RecyclerView.ViewHolder(binding.root) {
        fun getShibes(shibeImages: String) {
            binding.ivShibe.load(shibeImages)
        }

        companion object {
            fun newInstance(parent: ViewGroup) = ItemShibesBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            ).let { ShibeViewHolder(it) }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ShibeViewHolder =
        ShibeViewHolder.newInstance(parent)

    override fun onBindViewHolder(holder: ShibeViewHolder, position: Int) {
        val shibe = shibe[position]
        holder.getShibes(shibe)
    }

    override fun getItemCount(): Int = shibe.size

    fun displayShibes(shibe: List<String>) {
        this.shibe.run {
            val oldSize = size
            clear()
            notifyItemRangeRemoved(0, oldSize)
            addAll(shibe)
            notifyItemRangeInserted(0, size)
        }
    }
}