package com.example.petapp.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.example.petapp.databinding.ItemBirdBinding

class BirdAdapter : RecyclerView.Adapter<BirdAdapter.BirdViewHolder>() {
    private var bird = mutableListOf<String>()

    class BirdViewHolder(
        val binding: ItemBirdBinding,
    ) : RecyclerView.ViewHolder(binding.root) {
        fun getBirds(birdImages: String) {
            binding.ivBird.load(birdImages)
        }

        companion object {
            fun newInstance(parent: ViewGroup) = ItemBirdBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            ).let { BirdViewHolder(it) }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BirdViewHolder =
        BirdViewHolder.newInstance(parent)


    override fun onBindViewHolder(holder: BirdViewHolder, position: Int) {
        val bird = bird[position]
        holder.getBirds(bird)
    }

    override fun getItemCount(): Int = bird.size

    fun displayBirds(bird: List<String>) {
        this.bird.run {
            val oldSize = size
            clear()
            notifyItemRangeRemoved(0, oldSize)
            addAll(bird)
            notifyItemRangeInserted(0, size)
        }
    }
}