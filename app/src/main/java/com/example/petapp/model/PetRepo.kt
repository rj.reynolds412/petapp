package com.example.petapp.model

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

object PetRepo {

    private val petService = PetService.getInstance()

    suspend fun getCats() = withContext(Dispatchers.IO){
        return@withContext petService.getCats()
    }

    suspend fun getBirds() = withContext(Dispatchers.IO){
        return@withContext petService.getBirds()
    }

    suspend fun getShibes() = withContext(Dispatchers.IO){
        return@withContext petService.getShibes()
    }

}