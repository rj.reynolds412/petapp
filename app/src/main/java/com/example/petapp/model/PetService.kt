package com.example.petapp.model

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create
import retrofit2.http.GET
import retrofit2.http.Query

interface PetService {

    companion object {
        private const val BASE_URL = "https://shibe.online"
        private const val CAT_ENDPOINT = "/api/cats"
        private const val BIRD_ENDPOINT = "/api/birds"
        private const val SHIBE_ENDPOINT = "/api/shibes"

        fun getInstance(): PetService {
            val retrofit: Retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
            return retrofit.create()
        }

    }

    @GET(CAT_ENDPOINT)
    suspend fun getCats(@Query("count") count: Int = 100): List<String>

    @GET(BIRD_ENDPOINT)
    suspend fun getBirds(@Query("count") count: Int = 100): List<String>

    @GET(SHIBE_ENDPOINT)
    suspend fun getShibes(@Query("count") count: Int = 100): List<String>
}