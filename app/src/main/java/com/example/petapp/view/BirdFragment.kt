package com.example.petapp.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import com.example.petapp.adapter.BirdAdapter
import com.example.petapp.databinding.FragmentBirdBinding
import com.example.petapp.model.PetRepo
import com.example.petapp.viewModel.BirdViewModel

class BirdFragment : Fragment() {

    private var _binding: FragmentBirdBinding? = null
    private val binding get() = _binding!!
    private val birdViewModel by lazy { BirdViewModel(repo = PetRepo) }
    private val birdAdapter by lazy { BirdAdapter() }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ) = FragmentBirdBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initObservers()
    }

    private fun initObservers() = with(binding) {
        rvBirds.adapter = birdAdapter
        birdViewModel.getBirds()
        birdViewModel.birds.observe(viewLifecycleOwner){ state ->
            progress.isVisible = state.isLoading
            birdAdapter.displayBirds(state.birds)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}