package com.example.petapp.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.petapp.databinding.FragmentPetDirectoryBinding

class PetDirectoryFragment : Fragment() {

    private var _binding: FragmentPetDirectoryBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ) = FragmentPetDirectoryBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.birdIv.setOnClickListener {
            findNavController().navigate(
                PetDirectoryFragmentDirections.actionPetDirectoryFragmentToBirdFragment()
            )
        }
        binding.catIv.setOnClickListener {
            findNavController().navigate(
                PetDirectoryFragmentDirections.actionPetDirectoryFragmentToCatFragment()
            )
        }
        binding.shibeIv.setOnClickListener {
            findNavController().navigate(
                PetDirectoryFragmentDirections.actionPetDirectoryFragmentToShibeFragment()
            )
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}