package com.example.petapp.view

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.petapp.databinding.FragmentPetSplashBinding

class PetSplashFragment : Fragment() {

    private var _binding: FragmentPetSplashBinding? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ) = FragmentPetSplashBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val handler = Handler(Looper.getMainLooper())
        handler.postDelayed({
            findNavController().navigate(
                PetSplashFragmentDirections.actionPetSplashFragmentToPetDirectoryFragment())
        }, 5000)

    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}

