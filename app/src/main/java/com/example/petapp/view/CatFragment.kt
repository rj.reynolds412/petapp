package com.example.petapp.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import com.example.petapp.adapter.CatAdapter
import com.example.petapp.databinding.FragmentCatBinding
import com.example.petapp.model.PetRepo
import com.example.petapp.viewModel.CatViewModel

class CatFragment : Fragment() {

    private var _binding : FragmentCatBinding? = null
    private val binding get() = _binding!!
    private val catViewModel by lazy { CatViewModel(repo = PetRepo) }
    private val catAdapter by lazy { CatAdapter() }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentCatBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initObservers()
    }

    private fun initObservers() = with(binding) {
        rvCat.adapter = catAdapter
        catViewModel.getCats()
        catViewModel.cats.observe(viewLifecycleOwner){ state ->
            progress.isVisible = state.isLoading
            catAdapter.displayCats(state.cats)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}