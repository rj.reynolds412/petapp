package com.example.petapp.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import com.example.petapp.R
import com.example.petapp.adapter.ShibeAdapter
import com.example.petapp.databinding.FragmentShibeBinding
import com.example.petapp.model.PetRepo
import com.example.petapp.viewModel.ShibeViewModel

class ShibeFragment : Fragment() {

    private var _binding : FragmentShibeBinding? = null
    private val binding get() = _binding!!
    private val shibeViewModel by lazy { ShibeViewModel(repo = PetRepo)}
    private val shibeAdapter by lazy { ShibeAdapter() }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentShibeBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initObservers()
    }

    private fun initObservers() = with(binding) {
        rvShibes.adapter = shibeAdapter
        shibeViewModel.getShibes()
        shibeViewModel.shibes.observe(viewLifecycleOwner){ state ->
            progress.isVisible = state.isLoading
            shibeAdapter.displayShibes(state.shibes)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}