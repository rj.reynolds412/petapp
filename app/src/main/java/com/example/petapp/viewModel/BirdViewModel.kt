package com.example.petapp.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.petapp.model.PetRepo
import kotlinx.coroutines.launch

class BirdViewModel(val repo : PetRepo) : ViewModel() {

    private val _birds : MutableLiveData<BirdState> = MutableLiveData(BirdState(isLoading = true))
    val birds : LiveData<BirdState> get() = _birds

    fun getBirds(){
        viewModelScope.launch {
            _birds.value = BirdState(isLoading = true)
            val result = repo.getBirds()
            _birds.value = BirdState(isLoading = false, birds = result )
        }
    }


    data class BirdState(
        val isLoading: Boolean = false,
        val birds: List<String> = emptyList()
    )
}