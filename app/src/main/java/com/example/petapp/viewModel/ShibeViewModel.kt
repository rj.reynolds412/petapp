package com.example.petapp.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.petapp.model.PetRepo
import kotlinx.coroutines.launch

class ShibeViewModel(val repo: PetRepo) : ViewModel() {

    private val _shibe: MutableLiveData<ShibeState> = MutableLiveData(ShibeState(isLoading = true))
    val shibes : LiveData<ShibeState> get() =  _shibe

    fun getShibes(){
        viewModelScope.launch {
            _shibe.value = ShibeState(isLoading = false)
            val result = repo.getShibes()
            _shibe.value = ShibeState(isLoading = false, shibes = result)
        }
    }

    data class  ShibeState(
        val isLoading: Boolean = false,
        val shibes: List<String> = emptyList()
    )
}