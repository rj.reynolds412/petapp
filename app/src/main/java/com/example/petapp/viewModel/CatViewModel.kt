package com.example.petapp.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.petapp.model.PetRepo
import kotlinx.coroutines.launch

class CatViewModel(val repo: PetRepo) : ViewModel() {

    private val _cats: MutableLiveData<CatState> = MutableLiveData(CatState(isLoading = true))
    val cats : LiveData<CatState> get() = _cats

    fun getCats(){
        viewModelScope.launch {
            _cats.value = CatState(isLoading = true)
            val result = repo.getCats()
            _cats.value = CatState(isLoading = false, cats = result)
        }
    }

    data class CatState(
        val isLoading: Boolean = false,
        val cats: List<String> = emptyList(),
    )

}