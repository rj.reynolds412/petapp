package com.example.petapp.factory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.petapp.model.PetRepo
import com.example.petapp.viewModel.CatViewModel

class PetVMfactory ( private val repo: PetRepo
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return CatViewModel(repo) as T
    }
}